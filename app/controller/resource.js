var async = require('async'),
	mongoose = require('mongoose'),
	Resource = mongoose.model('Resource'),
	RoleResource = mongoose.model('RoleResource');

exports.showResource = function(req, res) {
	var item = req.query.item;
	var index = req.query.index;
	var begin = item * (index - 1);
	var end = item * index - 1;

	var resource = new Resource();
	var newData = [];

	resource.findResource(function(data) {
		if (item == null) {
			res.send(data);
		} else {
			console.log("resource data");
			var sendData = [];
			var k = 0;
			for (var i = begin; i < end + 1; i++) {
				if (i < data.length) {
					sendData[k] = data[i];
					k++;
				}

			}
			console.log("========sendData========");
			console.log(sendData);
			var getSendData = {
				count: data.length,
				data: sendData
			};
			res.send(getSendData);
		}
	});
};


// exports.pagesel = function (req, res) {
// 	console.log("role-resource-pagesel");
// 	var index = req.body.index;       //拿到第几页
// 	var item = req.body.item;         //拿到每页多少条
// 	var begin = item*(index-1);       //
// 	var end = item*index - 1;
//     var resource =[{name:"合同管理操作", resource:"/api/contracts"},
//     	           {name:"模版管理操作", resource:"/api/templates"},
//     	           {name:"代办管理任务", resource:"/api/tasks"},
//     	           {name:"用户角色分配",resource:"/api/userroles"},
//     	           {name:"角色管理操作",resource:"/api/roles"},
//     	           {name:"角色资源分配",resource:"/api/roleresource"},
//     	           {name:"用户操作日志",resource:"/api/userlog"},
//     	           {name:"合同修改历史",resource:"/api/businesslog"}];

//     var operation = [{oper:"post",name:"添加"},
//     	             {oper:"get",name:"查询"},
//     	             {oper:"put",name:"修改"},
//     	             {oper:"delete",name:"删除"}];
//     var sendData = [];
// 	var k = 0;
// 	var operaData = [];
// 	var m = 0;
// 	console.log("resource.length = " + resource.length);
// 	for (var i = begin; i < end+1; i++){
// 		if(i < resource.length) {
// 			sendData[k] = resource[i];
// 			k++;
// 		}
// 		if(i < operation.length) {
// 			operaData[m] = operation[i];
// 			m++;
// 		}
// 	}
// 	var getSendData = {
// 		count : resource.length,
// 		data : sendData,
// 		operaCount : operation.length, 
// 		operaData : operaData,
// 		allOpera : operation
// 	};
// 	console.log("getSendData"+getSendData.data[0].name);
// 	res.send(getSendData);

// }

exports.create = function(req, res) {
	var resource = req.body.resource;
	var url = req.body.url;

	var data = {
		resource: resource,
		url: url
	};

	var resourceModel = new Resource();
	resourceModel.insertData(data, function(tdata) {
		console.info(tdata);
		res.send(tdata);
	});
}

exports.update = function(req, res) {
	var resource = req.body.resource;
	var url = req.body.url;
	var rid = req.params['id'];

	var data = {
		resource: resource,
		url: url
	};

	//首先从resource表中拿到id为rid的资源，去roleresource中先更新与此资源相同的项，再更新resource表
	var resourceModel = new Resource();
	resourceModel.findResourceById(rid, function(result) {
		console.info("=====|||hahhahahahahha000====")
		console.info(result)
		var roleresource = new RoleResource();
		roleresource.findRoleResource(function(tdata) {
			for (var i = 0; i < tdata.length; i++) {
				var length = tdata[i].resource.length;
				var toUpdate = false;

				if (length != undefined) {
					for (var j = 0; j < length; j++) {

						if (tdata[i].resource[j]!=null && tdata[i].resource[j].opername == result[0].resource) {
							tdata[i].resource[j].resource = data.url;
							tdata[i].resource[j].opername = data.resource;

							toUpdate = true;
						}
					}
				}

				if (toUpdate) {
					var putData = {
						role: tdata[i].role,
						resource: tdata[i].resource
					};
					var rolename = tdata[i].role;
					roleresource.updateOneRoleResouce(rolename, putData, function(ttdata) {

						resourceModel.updateData(rid, data, function(tdata) {
							console.info(tdata);
							res.send(tdata);
						});
					});
				}
			}
		});
	});


}

exports.destroy = function(req, res) {
	var rdata = req.body;
	var resource = new Resource();
	var rid = req.params['id'];

	var roleresource = new RoleResource();

	roleresource.findRoleResource(function(tdata) {
		for (var i = 0; i < tdata.length; i++) {
			var length = tdata[i].resource.length;
			var toUpdate = false;
			var index;

			if (length != undefined) {
				for (var j = 0; j < length; j++) {

					if (tdata[i].resource[j] != null && tdata[i].resource[j].opername == rdata.resource) {

						index = j;
						toUpdate = true; //使用update作为delete作用
					}
				}
			}

			if (toUpdate) {
				if (index < length - 1) {
					for (; index < length; index++) {
						tdata[i].resource[index] = tdata[i].resource[index + 1];
						tdata[i].resource[length - 1] = null;
					}
				} else
					tdata[i].resource[index] = null;

				var putData = {
					role: tdata[i].role,
					resource: tdata[i].resource
				};

				console.info("====\\\\\|||||")
				console.info(putData)
				var rolename = tdata[i].role;
				roleresource.updateOneRoleResouce(rolename, putData, function(ttdata) {


					resource.removeResource(rid, function(data) {
						console.log("remove resource successfully");

						res.send("result:" + data);

					});
				});
			}
		}
	});

}
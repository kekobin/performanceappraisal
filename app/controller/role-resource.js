

var async = require('async'),
	mongoose = require('mongoose'),
	RoleResource = mongoose.model('RoleResource');
	UserRole = mongoose.model('UserRole');

exports.index = function(req,res){

	console.log("role-resource index");
	var roleResource = new RoleResource();
	roleResource.findRoleResource(function(data){
		console.log(data);
		res.send(data);
	});
};

exports.show = function(req,res){

	console.log("role-resource show");
	var roleResource = new RoleResource();
	var role=req.body.role;
	roleResource.findOneRoleResource(role, function(data){
		console.log(data);
		res.send(data);
	});
};

exports.create = function(req, res){
	
	console.log("role-resource creat");
	var getData = req.body;
	var roleResource = new RoleResource();
	var get = {
		role: getData.role,
		resource: getData.resource
	};
	console.log(get);
	roleResource.insertData(get,function(data){
		console.log(data);
		res.send(data);
	});
};

exports.update = function(req, res){

	console.log("role-resource update");
	var getData = req.body;
	console.log("original: ");
	console.log(getData);
	var rolename = getData.role;
	var roleResource = new RoleResource();
	var getResource1 = getData.resource;  //要把重复资源去除掉
	console.log("getResource1 come");
	console.log(getResource1);
	if(getResource1 == undefined){
		console.log("oh no");
		var get = {
			role: getData.role,
			resource: []
		};
		console.log("result: ")
		console.log(get);
		roleResource.updateOneRoleResouce(rolename, get, function(data){
			console.log(data);
			res.send(data);
		});
	}else{
		var resourceUnique = [];
		var r = 0;

	    for(var i=0;i<getResource1.length;i++){
	    	var temp = getResource1[i];
	    	var flag = 0;
	    	for(var j=0;j<resourceUnique.length;j++){
	    		if(temp.opername == resourceUnique[j].opername && temp.oper == resourceUnique[j].oper && temp.resource == resourceUnique[j].resource){
	    			flag=1;
	    			console.log("good");
	    		}
	    	}
	    	if(flag==0){
	    	    resourceUnique.push(getResource1[i]);
	        }
	    }

		var get = {
			role: getData.role,
			resource: resourceUnique
		};
		console.log("result: ")
		console.log(get);
		roleResource.updateOneRoleResouce(rolename, get, function(data){
			console.log(data);
			res.send(data);
		});
    }
};

exports.test = function(req, res){
	
	console.log("role-resource test");
	var getData = req.body;
	var rolename = "admin";
	var roleResource = new RoleResource();
	var get = {
		role: "admin",
		resource: [ { "opername" : "合同操作", "oper" : "post", "resource" : "/api/contracts" },
		            { "opername" : "合同操作", "oper" : "get", "resource" : "/api/contracts" } ]
	};
	console.log(get);
	roleResource.updateOneRoleResouce(rolename, get, function(data){
		console.log(data);
		res.send(data);
	});
};

exports.sidenav = function(req, res){
	var uid = req.user.uid;
	var userRole = new UserRole();
	var roleResource = new RoleResource();
	userRole.checkRole(uid, function(roles){
		var sendData = [];
		var index = 0;
		if(roles != null && roles.length !=0){
			roleResource.findRolesResource(roles, function(data) {
				 console.log("======sidenav======data==========");
				  console.log(data);
				
				if(data != null && data.length != 0){
					for(var i = 0; i < data.length; i++){
						var url = data[i].resource;
						sendData[index] = url;
						index++;
						
					}
				}
				console.log(sendData);
				res.send(sendData);
			});
		}else{
			res.send(null);
		}

	});
	
};

exports.userResources = function(req, res) {
	var uid = req.user.uid;
	var userRole = new UserRole();
	var roleResource = new RoleResource();
	userRole.checkRole(uid, function(roles) {

	});
};



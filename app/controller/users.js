/**
 * Module dependencies.
 */

var mongoose = require('mongoose'),
  User = mongoose.model('User'),
  UserRole = mongoose.model('UserRole'),
  generator = require('../generator/generator'),
  util = require('../util/common');


exports.index = function(req, res){

  console.log("user index");
  var user = new User();
  user.checkUserInfo(function(data){
    res.send(data);
  });
};

exports.pagesel = function(req, res) {
  console.log("pagesel");
  var index = req.body.index;       //拿到第几页
  var item = req.body.item;         //拿到每页多少条
  var begin = item*(index-1);       //
  var end = item*index - 1;
  var user = new User();
  var newData = [];
  var m = 0;
  user.checkUserInfo(function(data) {
    var sendData = [];
    var k = 0;
    for (var i = begin; i < end+1; i++){
      if(i < data.length) {
          sendData[k] = data[i];
          k++;
      }

    }
    console.log("========sendData========");
    console.log(sendData);
    var getSendData = {
      count : data.length,
      data: sendData
    };
    res.send(getSendData);

  });

};

exports.signin = function(req, res) {};

/**
 * Auth callback
 */

exports.authCallback = function(req, res, next) {
  res.redirect('/');
};


/**
 * check username
 */

exports.check = function(req, res) {
  var user = new User();
  console.log(req.body.username);
  user.checkUsername(req.body.username, function(data) {
    console.log(data);
    if (data != "")
      res.send("false");
    else
      res.send("true");
  });
};

/**
 * Show login form
 */

exports.login = function(req, res) {

  console.log('login', req.user);
  if (req.user != null || req.user != undefined) {
    return res.redirect('/desktop');
  } else {
    res.send(generator.generate('login', {
      username: 'Justin'
    }));
  }
};
/**
 * Show sign up form
 */

exports.signup = function(req, res) {
  res.send(generator.generate('login', {
    username: 'Justin'
  }));
};

/**
 * Logout
 */


exports.logout = function(req, res) {
  req.logout();
  res.redirect('/login');
};

/**
 * Session
 */

exports.session = function(req, res) {
  console.info("badboy:" + req.user);
  res.redirect('/desktop');
};

/**
 * Create user
 */

exports.create = function(req, res) {
    console.log(req.body);
    if (req.body['password'] != req.body['password2']) {
        req.flash("password error");
        return res.redirect('/login');
    }
    var rdata = req.body;
    var user = new User();
    var userRole = new UserRole();
    user.getUnique(function(data) { //生成唯一用户id
        console.log(data);
        var getUser = {
            uid: data,
            username: rdata.username,
            email: rdata.email,
            password: rdata.password
        };
        var getUserRole = {
            uid:data,
            username: rdata.username,
            role:rdata.role
        };
        user = new User(getUser);
        user.save(function(err) {
            if (err) {
                return res.send("fail insertUser");
            }
            else{
                userRole = new UserRole(getUserRole);
                userRole.save(function(error){
                if(error){
                    return res.send("fail insertUserRole");
                }
                return res.send('success insertUser and insertUserRole');
            });
        }
      // req.logIn(user, function(err) {
      //   if (err) {
      //     return 0;
      //   }
      //   return res.redirect('/desktop');
      // });
    });
  });
};

/**
 *  Show profile
 */

exports.show = function(req, res) {
  var user = req.profile;
  res.render('users/show', {
    title: user.name,
    user: user
  });
};

/**
 * Find user by id
 */

exports.user = function(req, res, next, id) {
  User
    .findOne({
    _id: id
  })
    .exec(function(err, user) {
    if (err) return next(err);
    if (!user) return next(new Error('Failed to load User ' + id));
    req.profile = user;
    next();
  });
};

exports.destroy = function(req, res, next){
  var rdata = req.body;
  var user = new User();
  var userRole = new UserRole();
  var uid = rdata.uid;
  user.removeUserData(uid,function(data){
    console.log("remove user successfully");
    userRole.removeData(uid,function(data){
      console.log("remove user-role successfully");
      res.send("result:"+data);
    });
  });
};

exports.update = function(req, res, next){
  var rdata = req.body;
  var user = new User();
  var userRole = new UserRole();
  var uid = rdata.uid;
  console.log("===");
  console.log(rdata);
  var getUser = {
      uid: rdata.uid,
      username: rdata.username,
      email: rdata.email,
      password: rdata.password
  };
  var getUserRole = {
      uid:rdata.uid,
      username: rdata.username,
      role:rdata.role
  };
  user.updateUser(uid, getUser, function(result1){
    console.log("update user successfully");
    console.log(result1);
    userRole.updateUserRole(uid,getUserRole, function(result2){
      console.log("update user-role successfully");
      console.log(result2);
      res.send("result:"+result2);
    });
  });
};

exports.pwdUpdate = function(req, res) {
  var uid = req.params['id'];
  var getUser = {
    uid: uid,
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  };

  var user = new User();

   user.updateUser(uid, getUser, function(result1){
      res.send("update successfully");
   });
};


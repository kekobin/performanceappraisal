var path = require('path'),
	rootPath = path.normalize(__dirname + '/..');

// 项目配置
module.exports = {
	development: {
		db: 'mongodb://localhost/personalAppraisal',
		root: rootPath,
		app: {
			name: 'PerformanceAppraisal'
		}
	},
	production: {
		db: 'mongodb://localhost/PerformanceAppraisal',
		root: rootPath,
		app: {
			name: 'PerformanceAppraisal'
		}
	}
};
var async = require('async'),
	resource = require('express-resource'),
	generator = require('../app/generator/generator'),
	mongoose = require('mongoose'),
	nodeExcel = require('excel-export');

////////contract-router
module.exports = function(app, passport, auth,notify, notifyApi) {

	
	//用户接口
	var users = require('../app/controller/users');
    
    app.get('/api/users',users.index);
	app.get('/login', users.login);
	app.get('/signup', users.signup);
	app.get('/logout', users.logout);
	app.post('/api/users', users.create);
	app.put('/api/users', users.update);
	app.put('/api/users/:id',users.pwdUpdate);
	app.del('/api/users',users.destroy);
	app.post('/username', users.check);
	app.post('/users/session', passport.authenticate('local', {

		failureRedirect: '/login',
		failureFlash: 'Invalid email or password.'

	}), users.session);
	app.post('/api/paginator',users.pagesel);//分页查询用户信息

	/////用户-角色分配接口  //app.post('/api/roles/:roleId/users', );...?
	var userRoles = require('../app/controller/user-role');

	app.get('/api/userroles',userRoles.index);
	app.get('/api/userroles/:id', userRoles.show);
	app.post('/api/userroles',userRoles.create);
	app.get('/api/roleusers',userRoles.getUser);

	//根据角色名称查看用户
	app.get('/api/roleusers/:rolename', userRoles.selUser);

    /////角色接口
	var roles = require('../app/controller/role');

	app.get('/api/roles',roles.index);
	app.get('/api/roles/:id', roles.show);
	app.post('/api/roles',roles.create);
	app.put('/api/roles/:id', roles.update);
	app.del('/api/roles/:id',roles.destroy);
	app.post('/roles/paginator', roles.pagesel);

	/////角色-资源分配接口
	var roleResource = require('../app/controller/role-resource');

	// app.get('/api/resource', roleResource.showResource);
	app.get('/api/roleresource',roleResource.index);
	app.post('/api/roleresource',roleResource.create);
	app.put('/api/roleresource',roleResource.update);
	app.get('/api/roleresource/:id',roleResource.show);
	app.get('/api/resourcetest', roleResource.test);
	// app.get('/api/roleresource/desktop/sidenav', roleResource.sidenav);

	// app.post('/api/resource', roleResource.pagesel);

	// /////资源分配接口
	var resource = require('../app/controller/resource');

	app.get('/api/resource',resource.showResource);
	app.post('/api/resource',resource.create);
	app.put('/api/resource/:id',resource.update);
	app.del('/api/resource/:id',resource.destroy);
	
	app.get('/', auth.requiresLogin, function(req, res) {
		res.redirect('/login');
	});

	app.get('/desktop', auth.requiresLogin,function(req, res) {
		res.send(generator.generate('desktop', {
			username: req.user.username,
		}));
	});

	app.get('/usermanage', auth.requiresLogin, function(req, res) {
		res.send(generator.generate('usermanage', {
			username: req.user.username
		}));
	});

	app.get('/rolemanage', auth.requiresLogin, function(req, res) {
		res.send(generator.generate('rolemanage', {
			username: req.user.username
		}));
	});

	app.get('/resourcemanage', auth.requiresLogin, function(req, res) {
		res.send(generator.generate('resourcemanage', {
			username: req.user.username
		}));
	});
	
	//菜单-授权管理
	app.get('/authorize', auth.requiresLogin, function(req, res) {
		res.send(generator.generate('authorize', {
			username: req.user.username
		}));
	});

};